function [Q1, Q2] = split_21(Q, p)
%SPLIT21 Split given matrix into 2x1 block matrix so that first block has p rows

  Q1 = Q(1:p, :);
  Q2 = Q(p+1:end, :);
end