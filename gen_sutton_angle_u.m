function X = gen_sutton_angle_u(n_half)
%GEN_SUTTON_ANGLE_U Generate orthogonal matrix with bidiagonal blocks generated
%using angles from uniform distribution, see @sutton2009

  if nargin < 1
    n_half = 20;
  end

  thetas = pi / 2 * rand(n_half, 1);
  phis = pi / 2 * rand(n_half-1, 1);
  X = gen_bidiagonal_block_form(thetas, phis);
end