function X = gen_bidiagonal_block_form(thetas, phis)
%GEN_BIDIAGONAL_BLOCK_FORM Generate bidiagonal block form described in
%@sutton2009, using angles theta_i and phi_i as parameters

  c = cos(thetas);
  s = sin(thetas);
  cc = cos(phis);
  ss = sin(phis);

  B11 = diag([c(1); c(2:end) .* cc]) - diag(s(1:end-1) .* ss, 1);
  B12 = diag([s(1:end-1) .* cc; s(end)]) + diag(c(2:end) .* ss, -1);
  B21 = -diag([s(1); s(2:end) .* cc]) - diag(c(1:end-1) .* ss, 1);
  B22 = diag([c(1:end-1) .* cc; c(end)]) - diag(s(2:end) .* ss, -1);

  X = [B11, B12; B21, B22];
end