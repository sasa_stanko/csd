function X = gen_sutton_cluster(n_half)
%GEN_SUTTON_CLUSTER Generate orthogonal matrix whose blocks have clustered
%singular values, see @sutton2009

  if nargin < 1
    n_half = 20;
  end

  deltas = 10.^(-18*rand(n_half+1, 1));
  deltas = cumsum(deltas);
  thetas = pi / 2 * deltas(1:n_half) / deltas(n_half+1);
  C = diag(cos(thetas));
  S = diag(sin(thetas));
  U1 = gen_rand_haar(n_half);
  U2 = gen_rand_haar(n_half);
  V1 = gen_rand_haar(n_half);
  V2 = gen_rand_haar(n_half);
  X = blkdiag(U1, U2) * [C, S; -S, C] * blkdiag(V1, V2).';
end