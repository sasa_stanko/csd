function [errs, worst_Q] = test_21(alg, generator, p, q, tests)
%TEST_22 Tests 2x1 CSD algorithm
%
%  alg       = Algorithm to test.
%  generator = Generator of random matrices.
%  p x q     = Dimension of block (1, 1).
%  test      = Number of test to perform

  errs = zeros(1, tests);
  worst_Q = [];
  worst_err = -1.0;
  for i = 1:tests
    Q = generator();
    [Q11, ~, Q21, ~] = split_22(Q, p, q);
    Q = [Q11; Q21];

    [U1, U2, V, C, S] = alg(Q11, Q21);

    norms = [norm(U1'*U1 - eye(size(U1, 1))),
             norm(U2'*U2 - eye(size(U2, 1))),
             norm(V'*V - eye(size(V, 1))),
             norm(U1'*Q(1:p,:)*V - C),
             norm(U2'*Q(p+1:end,:)*V - S)];

    err = max(norm(Q'*Q-eye(size(Q, 2))), eps(1));
    errs(i) = max(norms) / err;

    if errs(i) > worst_err
      worst_err = errs(i);
      worst_Q = Q;
    end
  end
end