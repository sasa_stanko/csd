function [errs, worst_Q] = test_22(alg, generator, p, q, tests)
%TEST_22 Tests 2x2 CSD algorithm
%
%  alg       = Algorithm to test.
%  generator = Generator of random matrices of fixed size.
%  p x q     = Dimension of block (1, 1).
%  test      = Number of test to perform

  errs = zeros(1, tests);
  worst_Q = [];
  worst_err = -1.0;

  % generate matrix Q earlier do detect dimension of the problem
  Q = generator();

  n = size(Q, 1);
  Sr = zeros(p, n-q);
  Sr(q+1:end,q+1:p) = eye(p-q);
  Cr = zeros(n-p, n-q);
  Cr(q+1:end,p+1:end) = eye(n-q-p);

  for i = 1:tests
    [Q11, Q12, Q21, Q22] = split_22(Q, p, q);
    [U1, U2, V1, V2, C, S] = alg(Q11, Q12, Q21, Q22);

    Sr(1:q,1:q) = -S(1:q,1:q);
    Cr(1:q,1:q) = C(1:q,1:q);

    norms = [norm(U1'*U1 - eye(size(U1, 2))),
             norm(U2'*U2 - eye(size(U2, 2))),
             norm(V1'*V1 - eye(size(V1, 2))),
             norm(V2'*V2 - eye(size(V2, 2))),
             norm(U1'*Q(1:p,1:q)*V1 - C),
             norm(U2'*Q(p+1:end,1:q)*V1 - S),
             norm(U1'*Q(1:p,q+1:end)*V2 - Sr),
             norm(U2'*Q(p+1:end,q+1:end)*V2 - Cr)];

    err = max(norm(Q'*Q-eye(size(Q, 1))), eps(1));
    errs(i) = max(norms) / err;

    if errs(i) > worst_err
      worst_err = errs(i);
      worst_Q = Q;
    end

    if i < tests
      Q = generator();
    end
  end
end