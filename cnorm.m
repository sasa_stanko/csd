function v = cnorm(X)
%CNORM Row vector of 2-norms for each column of the matrix
  n = size(X, 2);
  v = zeros(1, n);
  for i = 1:n
    v(i) = norm(X(:,i));
  end
end