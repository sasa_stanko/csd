function Q = gen_sutton_haar()
%GEN_SUTTON_HAAR Generate orthogonal matrix from Haar measure, see @sutton2009
%  Sutton suggests to divide the matrix into blocks with p = 18 and q = 15

  Q = gen_rand_haar(40);
end