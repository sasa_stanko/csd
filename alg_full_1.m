function [U1, U2, V1, V2, C, S] = alg_full_1(Q11, Q12, Q21, Q22)
%ALG_FULL_1 First attempt at full CSD algorithm
%           Assumes all blocks are squares

  [U1, U2, V1, V2, C, S] = alg_ecsd(Q11, Q12, Q21, Q22, @alg_vl_3);
end