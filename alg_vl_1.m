function [U1, U2, V, C, S] = alg_vl_1(Q1, Q2)
%ALG_VL_1 Algorithm 2.1 from @vanloan1985

  [U2, S, V] = svd(Q2);
  X = Q1 * V;
  C = cnorm(X);
  U1 = X * diag(1 ./ C);
  C = diag(C);
end