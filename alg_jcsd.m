function [U1, U2, V, C, S] = alg_jcsd(Q1, Q2, cs_approach)
%ALG_JCSD Computes Cosine-Sine decomposition of a 2x1 orthonormal matrix
%         using a variant of one-sided Jacobi algorithm.
%
%  Q1 = Upper block of orthonormal matrix
%  Q2 = Lower block of orthonormal matrix
%  cs_approach = Approach used to compute Jacobi rotations
%                (see find_c_and_s_csd for more information)

  if nargin < 3, cs_approach = 'default'; end

  n = size(Q1, 2);
  V = eye(n);
  sweep = 1;
  max_sweeps = 20;
  while sweep <= max_sweeps
    rots = 0;
    for i = 1:n-1
      for j = i+1:n
        [c, s, omit] = find_c_and_s_csd(Q1, Q2, i, j, cs_approach);
        if ~omit
          rots = rots + 1;
          Q1 = apply_from_right(Q1, i, j, c, s);
          Q2 = apply_from_right(Q2, i, j, c, s);
          V = apply_from_right(V, i, j, c, s);
        end
      end
    end
    sweep = sweep + 1;
    if rots == 0
      sweep = max_sweeps + 1;
    end
  end

  [U1, U2, V, C, S] = extract_matrices(Q1, Q2, V);
end


function A = apply_from_right(A, i, j, c, s)
  aux = A(:,i);
  A(:,i) = c * aux + s * A(:,j);
  A(:,j) = -s * aux + c * A(:,j);
end

function [U1, U2, V, C, S] = extract_matrices(Q1, Q2, V)
%EXTRACT_MATRICES Obtain CSD of matrix the [Q1; Q2] after JCSD was performed
%
%  Q1 = Matrix with orthogonal columns
%  Q2 = Matrix with orthogonal columns
%  V = Matrix of accumulated Jacobi transformations

  n = size(Q1, 2);
  C = cnorm(Q1);
  S = cnorm(Q2);

  % permute computed matrices so that diagonal of S is in ascending order
  [~, p] = sort(S);
  V = V(:,p);
  Q1 = Q1(:,p);
  Q2 = Q2(:,p);

  Ctmp = zeros(size(Q1));
  idcs = (size(Q1,1) * (0:(size(Q1,2)-1))) + (1:size(Q1,2));
  Ctmp(idcs) = C(p);
  C = Ctmp;
  Stmp = zeros(size(Q2));
  idcs = (size(Q2,1) * (0:(size(Q2,2)-1))) + (1:size(Q2,2));
  Stmp(idcs) = S(p);
  S = Stmp;

  [U1, R] = qr(Q1);
  U1 = [U1(:,1:n) * diag(2*(diag(R) >= 0) - ones(n, 1)), U1(:,n+1:end)];
  Q2 = Q2(:,end:-1:1);
  [U2, R] = qr(Q2);
  U2 = [U2(:,end:-1:n+1), U2(:,1:n) * diag(2*(diag(R) >= 0) - ones(n, 1))];
  U2 = U2(:,end:-1:1);
end
