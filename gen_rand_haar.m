function Q = gen_rand_haar(n)
%RAND_HAAR Generate random orthogonal matrix from Haar measure.

  [Q,~] = qr(randn(n));
  Q = Q * diag(sign(randn(n, 1)));
end