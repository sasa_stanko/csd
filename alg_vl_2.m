function [U1, U2, V, C, S] = alg_vl_2(Q1, Q2)
%ALG_VL_2 Algorithm 2.2 from @vanloan1985

  [U2, S, V] = svd(Q2);
  X = Q1 * V;
  [U1, R] = qr(X);
  C = diag(R);
  signs = ones(size(C)) - 2*(C < 0);  % avoid sign function because sign(0)=0
  U1 = U1 * diag(signs);
  C = diag(abs(C));
end