function verify_22(U1, U2, V1, V2, C, S, Q)
%VERIFY_22 Verify if computed 2x2 CSD is correct

  fprintf('\n');
  fprintf('   checking C: %.16g %.16g\n', min(diag(C)), max(diag(C)));
  fprintf('   checking S: %.16g %.16g\n', min(diag(S)), max(diag(S)));
  ccss = diag(C).^2 + diag(S).^2;
  fprintf('   checking C^2+S^2: %.16g %.16g\n', min(ccss), max(ccss));

  fprintf('   checking U1: %g\n', norm(U1'*U1 - eye(size(U1, 1))));
  fprintf('   checking U2: %g\n', norm(U2'*U2 - eye(size(U2, 1))));
  fprintf('   checking V1: %g\n', norm(V1'*V1 - eye(size(V1, 1))));
  fprintf('   checking V2: %g\n', norm(V2'*V2 - eye(size(V2, 1))));

  p = size(U1, 1);
  q = size(V1, 1);
  n = size(Q, 1);

  % create S and C blocks with eye matrices
  Sr = zeros(p, n-q);
  Sr(1:q,1:q) = -S(1:q,1:q);
  Sr(q+1:end,q+1:p) = eye(p-q);
  Cr = zeros(n-p, n-q);
  Cr(1:q,1:q) = C(1:q,1:q);
  Cr(q+1:end,p+1:end) = eye(n-q-p);

  fprintf('   checking U1''*Q11*V1 - C: %g\n', norm(U1'*Q(1:p,1:q)*V1 - C));
  fprintf('   checking U2''*Q21*V1 - S: %g\n', norm(U2'*Q(p+1:end,1:q)*V1 - S));
  fprintf('   checking U1''*Q12*V2 + S: %g\n', norm(U1'*Q(1:p,q+1:end)*V2 - Sr));
  fprintf('   checking U2''*Q22*V2 - C: %g\n', norm(U2'*Q(p+1:end,q+1:end)*V2 - Cr));
  fprintf('\n');
end