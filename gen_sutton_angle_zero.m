function X = gen_sutton_angle_zero(n_half)
%GEN_SUTTON_ANGLE_ZERO Generate orthogonal matrix with bidiagonal blocks
%generated using angles uniformly from {0, pi/4, pi/2}, see @sutton2009

  if nargin < 1
    n_half = 20;
  end

  thetas = pi / 2 * min(floor(3 * rand(n_half, 1)), 2);
  phis = pi / 2 * min(floor(3 * rand(n_half-1, 1)), 2);
  X = gen_bidiagonal_block_form(thetas, phis);
end