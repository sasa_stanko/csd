function [U1, U2, V1, V2, C, S] = alg_ecsd(Q11, Q12, Q21, Q22, csd21)
%ALG_ECSD Extended CSD algorithm
%         Assumes all blocks are squares
%
% csd21 = Function which can compute 2x1 CSD.
%         It should compute diagonal of S in ascending order

  [U1, U2, V1, C, S] = csd21(Q11, Q21);

  c = diag(C)';
  s = diag(S)';
  k = sum(c >= 0.5 * sqrt(2));
  q = length(c);
  V12C = Q22' * bsxfun(@rdivide, U2(:, 1:k), c(1:k));
  V12S = -Q12' * bsxfun(@rdivide, U1(:, k+1:q), s(k+1:end));
  V22 = Q12' * U1(:,q+1:end);
  V23 = Q22' * U2(:,q+1:end);
  V2 = [V12C, V12S, V22, V23];
end