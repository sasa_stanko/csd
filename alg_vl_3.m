function [U1, U2, V, C, S] = alg_vl_3(Q1, Q2)
%ALG_VL_3 Algorithm 3.1 from @vanloan1985

  % step 1
  [U2, S, V] = svd(Q2);
  Q2 = diag(flipud(diag(S)));
  U2 = fliplr(U2);
  V = fliplr(V);
  Q1 = Q1 * V;

  % step 2
  [U1, Q1] = qr(Q1);
  C = diag(Q1);
  signs = ones(size(C)) - 2*(C < 0);  % avoid sign function because sign(0)=0
  U1 = U1 * diag(signs);
  Q1 = diag(signs) * Q1;

  % step 3
  k = sum(diag(Q2) <= 0.5*sqrt(2));
  [U1til, C1, Vtil] = svd(Q1(k+1:end, k+1:end));
  U1(:, k+1:end) = U1(:, k+1:end) * U1til;
  V(:, k+1:end) = V(:, k+1:end) * Vtil;
  Q2(:, k+1:end) = Q2(:, k+1:end) * Vtil;

  % step 4
  S1 = cnorm(Q2(k+1:end, k+1:end)).';
  U2til = Q2(k+1:end, k+1:end) * diag(1 ./ S1);
  U2(:, k+1:end) = U2(:, k+1:end) * U2til;

  % C and S
  C = diag(Q1);
  C = diag([C(1:k); diag(C1);]);
  S = diag(Q2);
  S = diag([S(1:k); S1]);
end