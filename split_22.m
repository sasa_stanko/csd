function [Q11, Q12, Q21, Q22] = split_22(Q, p, q)
%SPLIT22 Split given matrix into 2x2 block matrix such that first block is pxq

  Q11 = Q(1:p, 1:q);
  Q12 = Q(1:p, q+1:end);
  Q21 = Q(p+1:end, 1:q);
  Q22 = Q(p+1:end, q+1:end);
end