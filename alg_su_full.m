function [U1, U2, V1, V2, C, S] = alg_su_full(Q11, Q12, Q21, Q22, csd21)
%ALG_SU_FULL Sutton's full CSD algorithm when 2x1 CSD is known, see @sutton2013
%            Assumes all blocks are squares
%
% csd21 = Function which can compute 2x1 CSD.
%         It should compute diagonal of S in ascending order

  [U1, U2, V1, C, S] = csd21(Q11, Q21);

	c = diag(C)';
  s = diag(S)';
  V2 = -Q12' * bsxfun(@times, U1, s) + Q22' * bsxfun(@times, U2, c);
end