function verify_21(U1, U2, V, C, S, Q)
%VERIFY_21 Verify if computed 2x1 CSD is correct

  fprintf('\n');
  fprintf('   checking C: %.16g %.16g\n', min(diag(C)), max(diag(C)));
  fprintf('   checking S: %.16g %.16g\n', min(diag(S)), max(diag(S)));
  ccss = diag(C).^2 + diag(S).^2;
  fprintf('   checking C^2+S^2: %.16g %.16g\n', min(ccss), max(ccss));

  fprintf('   checking U1: %g\n', norm(U1'*U1 - eye(size(U1, 1))));
  fprintf('   checking U2: %g\n', norm(U2'*U2 - eye(size(U2, 1))));
  fprintf('   checking V: %g\n', norm(V'*V - eye(size(V, 1))));

  p = size(U1, 1);
  fprintf('   checking U1''*Q1*V - C: %g\n', norm(U1'*Q(1:p,:)*V - C));
  fprintf('   checking U2''*Q2*V - S: %g\n', norm(U2'*Q(p+1:end,:)*V - S));
  fprintf('\n');
end