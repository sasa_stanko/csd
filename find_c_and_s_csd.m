function [c, s, omit] = find_c_and_s_csd(Q1, Q2, i, j, approach, cos_orth)
%FIND_C_AND_S_CSD Find cos and sin for rotation in CSD algorithm
%
%  omit = Flag to signal if it is safe to omit the rotation.
%  approach = Approach used to compute values c and s.
%             Possible values are 'default', 'rot', 'both'
%             Default value is 'default'.
%  cos_orth = If cosine of the angle between two columns is < cos_orth,
%             they are considered to be perpendicular.

  if nargin < 5
    approach = 'default';
  end
  if nargin < 6
    cos_orth = max(size(Q1, 1), size(Q2, 1)) * eps(1);
  end

  [a_ii, a_jj, a_ij] = get_cross(Q1, i, j);
  [b_ii, b_jj, b_ij] = get_cross(Q2, i, j);

  if strcmp(approach, 'rot') == 1
    [c, s, cos_angle] = compute_rot(a_ii, a_jj, a_ij, b_ii, b_jj, b_ij);
  elseif strcmp(approach, 'both') == 1
    [c, s, cos_angle] = compute_both(a_ii, a_jj, a_ij, b_ii, b_jj, b_ij);
  else
    [c, s, cos_angle] = compute_default(a_ii, a_jj, a_ij, b_ii, b_jj, b_ij);
  end

  omit = abs(cos_angle) < cos_orth;
end


% --- approaches to computing Jacobi rotation ---

function [c, s, cos_angle] = compute_default(a_ii, a_jj, a_ij, b_ii, b_jj, b_ij)
  c = 1;
  s = 0;

  % choose block from which cos and sin will be computed
  % prefer block whose columns have smaller norm
  if a_ii + a_jj < b_ii + b_jj
    cos_angle = sqrt(a_ii * a_jj);
    if cos_angle > 0
      cos_angle = a_ij / cos_angle;
      [c, s] = find_c_and_s(a_ii, a_jj, a_ij);
    end
  else
    cos_angle = sqrt(b_ii * b_jj);
    if cos_angle > 0
      cos_angle = b_ij / cos_angle;
      [c, s] = find_c_and_s(b_ii, b_jj, b_ij);
    end
  end
end

function [c, s, cos_angle] = compute_rot(a_ii, a_jj, a_ij, b_ii, b_jj, b_ij)
  c_a = 1;
  s_a = 0;
  t_a = 0;
  c_b = 1;
  s_b = 0;
  t_b = 0;

  % choose block from which cos and sin will be computed
  % prefer block whose columns have smaller norm
  cos_angle_a = sqrt(a_ii * a_jj);
  if cos_angle_a > 0
    cos_angle_a = a_ij / cos_angle_a;
    [c_a, s_a, t_a] = find_c_and_s(a_ii, a_jj, a_ij);
  end
  cos_angle_b = sqrt(b_ii * b_jj);
  if cos_angle_b > 0
    cos_angle_b = b_ij / cos_angle_b;
    [c_b, s_b, t_b] = find_c_and_s(b_ii, b_jj, b_ij);
  end

  sigma_prod_a = (a_ii + t_a*a_ij) * (a_jj - t_a*a_jj);
  sigma_prod_b = (b_ii + t_b*b_ij) * (a_jj - t_b*a_jj);

  if sigma_prod_a > sigma_prod_b
    cos_angle = cos_angle_b;
    c = c_b;
    s = s_b;
  else
    cos_angle = cos_angle_a;
    c = c_a;
    s = s_a;
  end
end

function [c, s, cos_angle] = compute_both(a_ii, a_jj, a_ij, b_ii, b_jj, b_ij)
  % Minimises sum of squares of off-diagonal elements

    cc = (a_ij^2 + b_ij^2) - 0.25 * ((a_ii - a_jj)^2 + (b_ii - b_jj)^2);
    dd = -(a_ij*(a_ii - a_jj) + b_ij*(b_ii - b_jj));

    if dd == 0
      c = 1;
      s = 0;
      cos_angle = 0;
      return
    end

    w = cc / dd;
    xi = abs(w) + sqrt(w^2 + 1);
    if sign(dd) * sign(w) < 0
      xi = 1 / xi;
    end
    if sign(dd) > 0
      xi = -xi;
    end

    t = xi / (1 + sqrt(xi^2 + 1));
    c = 1 / sqrt(1 + t^2);
    s = t / sqrt(1 + t^2);

    cos_angle_a = sqrt(a_ii * a_jj);
    if cos_angle_a > 0
      cos_angle_a = a_ij / cos_angle_a;
    end
    cos_angle_b = sqrt(b_ii * b_jj);
    if cos_angle_b > 0
      cos_angle_b = b_ij / cos_angle_b;
    end

    cos_angle = 0.5 * (cos_angle_a + cos_angle_b);
  end


% --- auxiliary functions ---

function [a_ii, a_jj, a_ij] = get_cross(Q, i, j)
  a_ii = dot(Q(:,i), Q(:,i));
  a_jj = dot(Q(:,j), Q(:,j));
  a_ij = dot(Q(:,i), Q(:,j));
end

function [c, s, t] = find_c_and_s(a_ii, a_jj, a_ij)
  mi = a_ii - a_jj;
  eta = 2.0 * a_ij * sign(mi);
  mi = abs(mi);
  if mi == 0
    t = sign(eta);
  else
    t = eta / (mi + sqrt(mi^2 + eta^2));
  end
  c = 1 / sqrt(1 + t^2);
  s = t / sqrt(1 + t^2);
end
