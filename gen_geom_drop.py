import mpmath
import numpy as np
import scipy.io

if __name__ == '__main__':
    # set precision to 40 digitt
    mpmath.mp.dps = 40

    # pick a from [0.2, 0.6]
    a = 0.2 + 0.4 * mpmath.rand()

    # compute n so that a**n < eps
    eps = 1e-16
    n = int(mpmath.ceil(mpmath.ln(eps) / mpmath.ln(a)))
    c = [a**k for k in range(n)]
    s = [mpmath.sqrt(1-x**2) for x in c]

    # compute random orthogonal factors
    U1 = mpmath.qr(mpmath.randmatrix(n))[0]
    U2 = mpmath.qr(mpmath.randmatrix(n))[0]
    VT = mpmath.qr(mpmath.randmatrix(n))[0]

    # form blocks
    Q1 = U1 * mpmath.diag(c) * VT
    Q2 = U2 * mpmath.diag(s) * VT

    # convert to numpy ndarray, reducing precision to 64 bits
    a = np.float64(a)
    Q1 = np.array(Q1.tolist(), dtype=np.float64)
    Q2 = np.array(Q2.tolist(), dtype=np.float64)

    # store in Matlab's .mat file
    scipy.io.savemat('geom_drop.mat', mdict={'a': a, 'n': n, 'Q1': Q1, 'Q2': Q2})